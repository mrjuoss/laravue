<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('todo', function() {
    return view('todo');
});

Route::get('customer', function() {
    return view('customer');
});

Route::get('/post', function() {
    $a = [
        [
            'title' => 'Belajar VueJS',
            'author'    => 'Daengweb.id'
        ],
        [
            'title' => 'Belajar Laravel',
            'author'    => 'Anugrah Sandi'
        ],
        [
            'title' => 'Belajar Javascript',
            'author'    => 'Daengweb.id'
        ],
        [
            'title' => 'Belajar PHP',
            'author'    => 'Daengweb.id'
        ],
        [
            'title' => 'Belajar HTML',
            'author'    => 'Daengweb.id'
        ]
    ];
    return $a;
});
