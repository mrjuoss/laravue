<?php

use App\Http\Controllers\TodoController;
use App\Http\Controllers\CustomerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('todo', [TodoController::class, 'index']);
Route::post('todo', [TodoController::class, 'store']);
Route::post('todo/change-done-status/{id}', [TodoController::class, 'changeDoneStatus']);
Route::post('todo/delete/{id}', [TodoController::class, 'destroy']);

Route::get('customer', [CustomerController::class, 'index']);
Route::get('customer/{id}', [CustomerController::class, 'show']);
Route::post('customer', [CustomerController::class, 'store']);
Route::put('customer/update/{id}', [CustomerController::class, 'update']);
Route::post('customer/delete/{id}', [CustomerController::class, 'destroy']);
