<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
​
        <title>Ajax Request With Axios - Daengweb</title>
​
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>
    <body>
       <div id="dw">
          ​<ul v-for="post in posts">
              <li><strong>@{{ post.title }}</strong>, Penulis: <i>@{{ post.author }}</i></li>
          </ul>
       </div>
​
      <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
      <script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
      <script type="text/javascript">
          new Vue({
               el: '#dw',
               data: {
                  posts: []
               },
               mounted() {
                   // axios.get('/post').then(response => console.log(response));
                   axios.get('/post').then(response => this.posts = response.data);
               }
           })
      </script>
    </body>
</html>