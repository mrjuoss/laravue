<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <title>Todo List</title>
    <style>
        .complete {
            text-decoration: line-through;
        }
    </style>
</head>
<body>
    <div id="app">
        <input type="text" v-model="newTodo" @keyup.enter="addTodo"> <button @click="addTodo">Add</button>
        <ul>
            <li v-for="(todo, index) in todos" :class="{complete: todo.done }">
                @{{ todo.text }}
                <button @click="removeTodo(index)">x</button>
                <button @click="toogleDone(todo)">Done</button>
            </li>
        </ul>
    </div>
    <script src="{{ asset('js/axios.min.js') }}"></script>
    <script src="{{ asset('js/vue.js') }}"></script>
    <script type="text/javascript">
       new Vue({
            el: "#app",
            data: {
                newTodo: '',
                todos: []
            },
            methods: {
                addTodo: function() {
                    let textInput = this.newTodo.trim()
                    if(textInput) {
                        axios.post('/api/todo', {
                            text: textInput
                        }).then(response => {
                             this.todos.unshift({
                                text: textInput,
                                done: 0
                             })
                        })
                        // this.todos.unshift({
                        //     text: textInput,
                        //     done: 0
                        // })
                        this.newTodo = ''
                    }
                },
                removeTodo: function(index) {
                    console.log(index)
                    this.todos.splice(index, 1)
                },
                toogleDone: function(todo) {
                    todo.done = !todo.done
                }
            },
            mounted() {
                axios.get('/api/todo').then(response => {
                    this.todos = response.data.data
                });
            }
        });
    </script>
</body>
</html>