<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <title>Customer List</title>
</head>
<body>
    <div id="app">
      
            <input type="text" v-model="name" @keyup.enter="addCustomer" size="50"> 
            <button v-show="!isEdit" @click="addCustomer">Add</button>
            <button v-show="isEdit" @click="updateCustomer">Update</button>
       
        
        <ul>
            <li v-for="(customer, index) in customers">
                @{{ customer.name }}
                <button @click="editCustomer(customer)">Edit</button> ||
                <button @click="removeCustomer(customer, index)">Delete</button>
            </li>
        </ul>
    </div>
    <script src="{{ asset('js/axios.min.js') }}"></script>
    <script src="{{ asset('js/vue.js') }}"></script>
    <script type="text/javascript">
       new Vue({
            el: "#app",
            data: {
                customers: [],
                name: '',
                isEdit: false,
                id_customer: ''
            },
            methods: {
                addCustomer: function() {
                    let inputName = this.name.trim();
                    if(inputName) {
                        axios.post('/api/customer', {
                            name: inputName
                        }).then(response => {
                             this.customers.unshift({
                                name: inputName
                             })
                        });
                        this.name = '';
                    }
                },
                removeCustomer: function(customer, index) {
                    let id = customer.id;
                    let result = confirm("Apakah anda yakin ?");
                    if (result) {
                        axios.post('/api/customer/delete/'+id)
                             .then(response => {
                                this.customers.splice(index, 1);
                         });
                    }
                    
                },
                editCustomer: function(customer) {
                    this.isEdit = true;
                    this.id_customer = customer.id;
                    axios.get('/api/customer/'+this.id_customer)
                         .then(response => {
                            this.name = response.data.data.name;
                         })
                    console.log(this.id_customer);
                },
                updateCustomer: function() {
                    let inputName = this.name.trim();
                    if(inputName) {
                        axios.put('/api/customer/update/'+this.id_customer, {
                                name: inputName
                             })
                             .then(response => {
                                axios.get('/api/customer').then(response => {
                                    this.customers = response.data.data;
                                });
                                this.name = '';
                                this.isEdit = false;
                                alert("Data updated successfully");
                            });
                    }
                }
            },
            mounted() {
                axios.get('/api/customer').then(response => {
                    this.customers = response.data.data;
                    console.log(this.customers);
                });
            }
        });
    </script>
</body>
</html>